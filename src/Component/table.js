import React, { Component } from 'react'
import { Table, Button } from 'antd'
import { POLICIES, SEARCHPOLICIES } from '../service/policies'
import DetailModal from './detail-modal'
import {LeftOutlined, RightOutlined }  from '@ant-design/icons'
import { Input , Select } from 'antd';
const { Search } = Input;
const {Option} = Select
class DataTable extends Component {
    state = {
        data: [],
        detail : false,
        page : 1,
        searchType : "policy_id",
        searchValue : ""
    }
    componentDidMount() {
        try {
            POLICIES(1).then(res => {
                this.setState({
                    data: res.response.result.policies
                })
            })
        } catch (err) {
            console.log(err)
        }
    }
    handleDetail = (data) => {
        if(data == "updated") {
            try {
                POLICIES(this.state.page).then(res => {
                    this.setState({
                        data: res.response.result.policies
                    })
                })
            } catch (err) {
                console.log(err)
            }
            this.setState({
                detail:false
            })
        }
        else {
            this.setState({
                detail:data
            })
        }
        
    }

    handlePage = (data) => {
        if(data=="next") {
            try {
                POLICIES(this.state.page + 1).then(res => {
                    this.setState({
                        data: res.response.result.policies
                    })
                })
            } catch (err) {
                console.log(err)
            }
            this.setState({
                page:this.state.page + 1
            })
        }
        else {
            try {
                POLICIES(this.state.page - 1).then(res => {
                    this.setState({
                        data: res.response.result.policies
                    })
                })
            } catch (err) {
                console.log(err)
            }
            this.setState({
                page:this.state.page - 1
            })
        }
    }
    searchType = (value) => {
        this.setState ({
            searchType : value
        })
    }
    onSearch = (value) => {
        console.log(value)
        this.setState({
            searchValue : value
        })
        try {
            SEARCHPOLICIES(this.state.searchType, value).then(res=>{
                this.setState({
                    data: [res.response.result]
                })
            })
        }catch(e){
            console.log("1")
        }
        

    }
    render() {
        const columns = [
            {
                title: 'Policy ID',
                dataIndex: 'policy_id',
                key: 'policy_id',
            },
            {
                title: 'Date of Purchase',
                dataIndex: 'date_of_purchase',
                key: 'date_of_purchase',
            },
            {
                title: 'Customer ID',
                dataIndex: 'customer_id',
                key: 'customer_id',
            },
            {
                title: 'Premium',
                dataIndex: 'premium',
                key: 'premium',
            },
            {
                title: "Action",
                key: 'customer_id',
                render: (text, record) => <Button onClick={()=>this.handleDetail(record)} type="primary" > Action </Button>,
            }
        ];
        return (
            <React.Fragment>
                <div style={{display:"flex"}} >
                <Select onChange={this.searchType} defaultValue="policy_id">
                    <Option key="policy_id" >
                        Policy ID
                    </Option>
                    <Option key="customer_id" >
                        Customer ID
                    </Option>
                </Select> 
                <Search placeholder="Search" onSearch={this.onSearch} enterButton />
                </div>
                <Table pagination={false} dataSource={this.state.data} columns={columns} />
                <br/>
                <div style={{float:"right"}} >
                    <Button onClick={()=>this.handlePage("prev")} disabled={this.state.page==1?true:false} type="primary" ><LeftOutlined /></Button>
                    &nbsp; &nbsp;
                    <Button disabled type="primary" > {this.state.page} </Button>
                    &nbsp; &nbsp;
                    <Button onClick={()=>this.handlePage("next")} type="primary" ><RightOutlined /></Button>
                </div>
                <div style={{clear:"both"}} ></div>
                {this.state.detail && <DetailModal detail={this.state.detail} handleModal={this.handleDetail} />}
            </React.Fragment>
        )
    }
}

export default DataTable