import React, { Component } from 'react'
import { Button, Tag, Input } from 'antd'
import { CloseOutlined } from '@ant-design/icons'
import {UPDATEPOLICIES} from '../service/policies'
class DetailModal extends Component {
    state ={
        premium : this.props.detail.premium
    }
    componentDidMount() {
        console.log(this.props)
    }
    update = (data, premium) =>{
        try {
            UPDATEPOLICIES(data, this.state.premium).then(res=>{
                console.log(res)
                this.props.handleModal("updated")
            })
        }catch(e){
            console.log(e)
        }
    }
    handlePremium = (e) => {
        this.setState({
            premium : e.target.value
        })
    }
    render() {
        return (
            <React.Fragment>
                <div onClick={() => this.props.handleModal(false)} className="detail-modal-background" > </div>
                <div className="detail-modal" >
                    <Button type="primary" onClick={() => this.props.handleModal(false)} style={{ position: "absolute", right: "0", bottom: "100%" }} > <CloseOutlined /> </Button>
                    <div style={{ width: "100%", display: "flex", margin:"1%" }} >
                        <div style={{ width: "32%" }} >
                            <div className="dashboard-data" >
                                <center>
                                   Policy ID &nbsp; : &nbsp; {this.props.detail.policy_id}
                                </center>
                            </div>
                        </div>
                        <div style={{ width: "34%" }} >
                            <div className="dashboard-data" >
                                <center>
                                   Customer ID &nbsp; : &nbsp; {this.props.detail.customer_id}
                                </center>
                            </div>
                        </div>
                        <div style={{ width: "32%" }} >
                            <div className="dashboard-data" >
                                <center>
                                Date of Purchase &nbsp; : &nbsp; {this.props.detail.date_of_purchase}
                                </center>
                            </div>
                        </div>
                    </div>
                    <h2 style={{color:"white", margin:"2%"}} > Customer Detail </h2>
                    <div style={{ width: "100%", display: "flex", margin:"1%" }} >
                        <div style={{ width: "32%" }} >
                            <div className="dashboard-data" >
                                <center>
                                   Gender &nbsp; : &nbsp; {this.props.detail.customer_gender}
                                </center>
                            </div>
                        </div>
                        <div style={{ width: "32%" }} >
                            <div className="dashboard-data" >
                                <center>
                                Customer Marital Status &nbsp; : &nbsp; {this.props.customer_marital_status?"Married":"Unmarried"}
                                </center>
                            </div>
                        </div>
                        <div style={{ width: "34%" }} >
                            <div className="dashboard-data" >
                                <center>
                                Customer Region &nbsp; : &nbsp; {this.props.detail.customer_region}
                                </center>
                            </div>
                        </div>
                    </div>
                    <h2 style={{color:"white", margin:"2%"}} > Vehicle & Policy Detail </h2>
                    <div style={{ width: "100%", display: "flex", margin:"1%" }} >
                        <div style={{ width: "32%" }} >
                            <div className="dashboard-data" >
                                <center>
                                Fuel &nbsp; : &nbsp; {this.props.detail.Fuel}
                                </center>
                            </div>
                        </div>
                        <div style={{ width: "32%" }} >
                            <div className="dashboard-data" >
                                <center>
                                Bodily Injury Liability &nbsp; : &nbsp; {this.props.detail.bodily_injury_liability}
                                </center>
                            </div>
                        </div>
                        <div style={{ width: "34%" }} >
                            <div className="dashboard-data" >
                                <center>
                                Collision &nbsp; : &nbsp; {this.props.detail.collision}
                                </center>
                            </div>
                        </div>
                    </div>
                    <div style={{ width: "100%", display: "flex", margin:"1%" }} >
                        <div style={{ width: "32%" }} >
                            <div className="dashboard-data" >
                                <center>
                                Comprehensive &nbsp; : &nbsp; {this.props.detail.comprehensive}
                                </center>
                            </div>
                        </div>
                        <div style={{ width: "32%" }} >
                            <div className="dashboard-data" >
                                <center>
                                Personal Injury Protection &nbsp; : &nbsp; {this.props.detail.personal_injury_protection}
                                </center>
                            </div>
                        </div>
                        <div style={{ width: "34%" }} >
                            <div className="dashboard-data" >
                                <center>
                                Property Damage Liability &nbsp; : &nbsp; {this.props.detail.property_damage_liability}
                                </center>
                            </div>
                        </div>
                    </div>
                    <div style={{ width: "100%", display: "flex", margin:"1%" }} >
                        <div style={{ width: "32%" }} >
                            <div className="dashboard-data" >
                                <center>
                                Vehicle Segment &nbsp; : &nbsp; {this.props.detail.vehicle_segment}
                                </center>
                            </div>
                        </div>
                        <div style={{ width: "66%" }} >
                           
                                 &nbsp; &nbsp; <b style={{color:"#497EE1"}} >Premium</b>
                                <Input defaultValue={this.props.detail.premium} style={{margin:"1%", width:"70%"}} onChange={this.handlePremium} ></Input>
                                <Button onClick={()=>this.update(this.props.detail.policy_id)} type="primary" > Update </Button>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default DetailModal