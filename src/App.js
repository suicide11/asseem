import React, { Component } from 'react'
import './App.css'
import 'antd/dist/antd.css';
import DataTable from './Component/table'
import * as Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import {POLICYCOUNT} from './service/policies'
import {Select} from 'antd'
const {Option} = Select

class App extends Component {
  state = {
    key : [],
    value : []
  }
  componentDidMount () {
    var key = []
    var value = []
    try {
      POLICYCOUNT("East").then(async res=>{
        console.log(res)
       await Promise.all(res.response.result.map(val=>{
          key.push(val._id)
          value.push(val.number_of_policies)
        }))
        this.setState({
          key : key,
          value : value
        })
      })
    }catch(e){
      console.log(e)
    }
  }
  handleRegion = (data)=> {
    console.log(value)
    var key = []
    var value = []
    try {
      POLICYCOUNT(data).then(async res=>{
        console.log(res)
       await Promise.all(res.response.result.map(val=>{
          key.push(val._id)
          value.push(val.number_of_policies)
        }))
        this.setState({
          key : key,
          value : value
        })
      })
    }catch(e){
      console.log(e)
    }
  }
  render() {
    console.log(this.state)
    const options = Highcharts.Options = {
      title: {
        text: 'My chart',
        style: { color: "black" }
      },
      xAxis: {
        categories: this.state.key,
      },
      series: [{
        type: 'column',
        data: this.state.value
      }],
      chart: {
        backgroundColor: "black",
        style: { color: "white" }
      },
      plotOptions: {
        series: {
          dataLabels: {
            enabled: true,
            color: 'white'
          }
        }
      },
    }
    return (
      <React.Fragment>
        <center>
          <h1 style={{color:"white"}} >
            Policy Detail
          </h1>
        </center>
        <div className="dashboard-data" >
          <DataTable />
        </div>

        <center>
          <h1 style={{color:"white"}} >
            Statistics
          </h1>
        </center>
        <div className="dashboard-data" >
          Region &nbsp; : &nbsp;<Select onChange={this.handleRegion}  style={{width:"10%"}} defaultValue="East">
            <Option key = "East">
              East
            </Option>
            <Option key = "West">
              West
            </Option>
            <Option key = "North">
              North
            </Option>
            <Option key = "South">
              South
            </Option>
          </Select>
          <br/> <br/>
          <HighchartsReact
            highcharts={Highcharts}
            options={options}
          />
        </div>
        
      </React.Fragment>
    )
  }
}

export default App