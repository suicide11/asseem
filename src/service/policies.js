import axios from 'axios'
import {BASEURL} from './rootURL'
const POLICIES = (data) => {
    return axios(BASEURL+'policy?limit=10&page='+data, {
        method: 'GET',
    })
        .then(response => response.data)
        .catch(error => {
            throw error;
        });
}

const UPDATEPOLICIES = (data, premium) => {
    return axios(BASEURL+'policy/'+data, {
        method: 'PUT',
        data:{premium:premium}
    })
        .then(response => response.data)
        .catch(error => {
            throw error;
        });
}


const SEARCHPOLICIES = (type, value) => {
    return axios(BASEURL+'policy/search?'+type+ '='+value, {
        method: 'GET',
    })
        .then(response => response.data)
        .catch(error => {
            throw error;
        });
}

const POLICYCOUNT = (data) => {
    return axios(BASEURL+'policy/count?zone='+data, {
        method: 'GET',
    })
        .then(response => response.data)
        .catch(error => {
            throw error;
        });
}

export {POLICIES, UPDATEPOLICIES, SEARCHPOLICIES, POLICYCOUNT}